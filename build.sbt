name := "WankinTime"

version := "1.0"

scalaVersion := "2.11.8"

assemblyJarName in assembly := "wankinTime.jar"

mainClass in (Compile, run) := Some("com.siliconvalley.Main")

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"
