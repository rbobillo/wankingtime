package com.siliconvalley.utils

import java.io.File

object ConfigValidator {

  def validateConfig[T](json: String): Option[T] =
    util.parsing.json.JSON.parseFull(json) match {
      case Some(js) => Some(js.asInstanceOf[T])
      case None     => Log.errorO[T]("invalid JSON")
    }

  def validateFile[T](path: File): Option[T] =
    if (path.exists()) validateConfig[T](io.Source.fromFile(path).mkString)
    else Log.errorO[T](s"$path: file not found")

  def validateFile[T](path: String): Option[T] =
    validateFile[T](new File(path))

}