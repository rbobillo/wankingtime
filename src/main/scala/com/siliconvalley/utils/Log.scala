package com.siliconvalley.utils

/**
  * Created by rbobillot on 10/04/17.
  */
object Log {

  private val removeHeadInMessage: (String, String) => String =
    (msg,_) => msg.dropWhile(_ != ':').stripPrefix(":").trim

  private val addHeadInMessage: (String, String) => String =
    (msg,head) => head + ": " + msg

  private val messageContainsHead: (String, String) => Boolean =
    (msg,head) => msg.toLowerCase.startsWith(head.toLowerCase + ":")

  private def cleanMessage(head: String)(msg: String): String =
    if (! messageContainsHead(msg, head)) msg
    else List(removeHeadInMessage, addHeadInMessage).foldLeft(msg)((res, f) => f(res,head))


  def error(msg: String): Unit =
    println( cleanMessage("Error")(msg) )

  def errorO[T](msg: String, opt: Option[T] = None): Option[T] = {
    println( cleanMessage("Error")(msg) )
    opt
  }


  def warn(msg: String): Unit =
    println( cleanMessage("Warn")(msg) )

  def warnO[T](msg: String, opt: Option[T] = None): Option[T] = {
    println( cleanMessage("Warn")(msg) )
    opt
  }
}
