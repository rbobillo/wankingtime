package com.siliconvalley

import com.siliconvalley.compute.Computor
import scala.util.{Failure, Success, Try}

object Main {

  val usage = "Please give a number of wankersNumber, and a number of peopleNumber to be wanked"

  def validateAsInts(wankersNumber: String, peopleNumber: String): Try[(Int, Int)] =
    for {
      w <- Try(wankersNumber.toInt)
      p <- Try(peopleNumber.toInt)
    } yield
      w -> p

  def showWankingTime(wankersNumber: String, peopleNumber: String, file: Option[String] = None): Unit =
    validateAsInts(wankersNumber, peopleNumber) match {
      case Success((w,p)) => Computor.estimateWankingTime(w,p,file)
      case Failure(error) => println(usage)
    }

  def main(av: Array[String]): Unit = av match {
    case Array(w,p)   => showWankingTime(w,p)
    case Array(w,p,f) => showWankingTime(w,p,Some(f))
    case _            => println(usage)
  }

}
