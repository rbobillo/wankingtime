package com.siliconvalley.compute

import com.siliconvalley.props.People
import com.siliconvalley.utils.ConfigValidator


object Computor {

  type Config  = Map[String,_]
  type ConfigO = Option[Config]

  def getConfigForResource(file: Option[String]): ConfigO =
    ConfigValidator.validateFile[Config](file match {
      case Some(path) => path
      case None       => getClass.getResource("/people.json").getFile
    })

  def estimateWankingTime(wankersNumber: Int, peopleNumber: Int, file: Option[String]): Unit = {
    implicit val config: ConfigO = getConfigForResource(file)
    val peeps: Array[People]     = PeopleCreator generate peopleNumber

    println( peeps.mkString("\n") )
  }

}
