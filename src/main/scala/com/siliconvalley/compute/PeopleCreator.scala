package com.siliconvalley.compute

import com.siliconvalley.props.{People, Wand}

object PeopleCreator {

  type Config  = Map[String,_]
  type ConfigO = Option[Config]

  def getRandElementFromCollection[T](it: Iterable[T]): T =
    it.toSeq( util.Random.nextInt(it.size) )

  def findKey(key: String, cfg: Config): Option[_] =
    cfg get key match {
      case Some(result) => Some(result)
      case None =>
        println(s"Error: $key: value not found in config")
        None
    }

  def pickValueFromConfigO(value: String, cfg: Config): Option[Int] =
    for {
      list   <- findKey(value, cfg)
      bounds <- Some(list.asInstanceOf[List[Double]].map(_.toInt))
      range  <- Some(bounds.head to bounds.last)
    } yield
      getRandElementFromCollection(range)

  def createRandomWandO(cfg: Config): Option[Wand] =
    for {
      wand  <- findKey("wand", cfg)
      size  <- pickValueFromConfigO("size", wand.asInstanceOf[Config])
      wfd   <- pickValueFromConfigO("wfd", wand.asInstanceOf[Config])
      curve <- pickValueFromConfigO("curve", wand.asInstanceOf[Config])
    } yield
      Wand(size, wfd, curve)

  def createRandomPeopleO(configO: ConfigO): Option[People] =
    for {
      cfg         <- configO
      wand        <- createRandomWandO(cfg)
      age         <- pickValueFromConfigO("age", cfg)
      sensitivity <- pickValueFromConfigO("sensitivity", cfg)
      openMinded  <- pickValueFromConfigO("openMinded", cfg) if openMinded > 0
    } yield
      People(age, wand, sensitivity, openMinded)

  def generate(peeps: Int)(implicit configO: ConfigO = None): Array[People] =
    (1 to peeps).par.flatMap(_ => createRandomPeopleO(configO)).toArray

}
