package com.siliconvalley.props

case class Wand(size: Int, wfd: Int, curve: Int)