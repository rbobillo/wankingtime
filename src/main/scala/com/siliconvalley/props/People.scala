package com.siliconvalley.props

case class People(age: Int, wand: Wand, sensitivity: Int, openMinded: Int)
